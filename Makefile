# GNU Make workspace makefile autogenerated by Premake

.NOTPARALLEL:

ifndef config
  config=debug
endif

ifndef verbose
  SILENT = @
endif

ifeq ($(config),debug)
  Glad_config = debug
  ImGui_config = debug
  MinecraftClone_config = debug
endif
ifeq ($(config),release)
  Glad_config = release
  ImGui_config = release
  MinecraftClone_config = release
endif

PROJECTS := Glad ImGui MinecraftClone

.PHONY: all clean help $(PROJECTS) 

all: $(PROJECTS)

Glad:
ifneq (,$(Glad_config))
	@echo "==== Building Glad ($(Glad_config)) ===="
	@${MAKE} --no-print-directory -C MinecraftClone/vendor/glad -f Makefile config=$(Glad_config)
endif

ImGui:
ifneq (,$(ImGui_config))
	@echo "==== Building ImGui ($(ImGui_config)) ===="
	@${MAKE} --no-print-directory -C MinecraftClone/vendor/imgui -f Makefile config=$(ImGui_config)
endif

MinecraftClone: Glad ImGui
ifneq (,$(MinecraftClone_config))
	@echo "==== Building MinecraftClone ($(MinecraftClone_config)) ===="
	@${MAKE} --no-print-directory -C MinecraftClone -f Makefile config=$(MinecraftClone_config)
endif

clean:
	@${MAKE} --no-print-directory -C MinecraftClone/vendor/glad -f Makefile clean
	@${MAKE} --no-print-directory -C MinecraftClone/vendor/imgui -f Makefile clean
	@${MAKE} --no-print-directory -C MinecraftClone -f Makefile clean

help:
	@echo "Usage: make [config=name] [target]"
	@echo ""
	@echo "CONFIGURATIONS:"
	@echo "  debug"
	@echo "  release"
	@echo ""
	@echo "TARGETS:"
	@echo "   all (default)"
	@echo "   clean"
	@echo "   Glad"
	@echo "   ImGui"
	@echo "   MinecraftClone"
	@echo ""
	@echo "For more information, see https://github.com/premake/premake-core/wiki"