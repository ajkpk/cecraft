#pragma once

#include "Core.h"
#include "events/Event.h"

class Layer
{
    protected:
        std::string m_DebugName;

    public:
        Layer(const std::string& name = "Layer");
        virtual ~Layer();

        virtual void onAttach() {}
        virtual void onDetach() {}
        virtual void onUpdate() {}
        virtual void onEvent(Event& e) {}

        inline const std::string& getName() const { return m_DebugName; }
};