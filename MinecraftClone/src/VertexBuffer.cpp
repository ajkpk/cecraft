#include "VertexBuffer.h"
#include "Renderer.h"
#include "Core.h"
#include <glad/glad.h>


VertexBuffer::VertexBuffer(const void* data, unsigned int size)
{
    //Create a Buffer, write its id into an int
	GLCall(glGenBuffers(1, &m_RendererID));
	//Select the buffer
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_RendererID));
	//Set the data to the buffer, after telling opengl how big it is
	GLCall(glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW));
}

VertexBuffer::~VertexBuffer()
{
    GLCall(glDeleteBuffers(1, &m_RendererID));
}

void VertexBuffer::Bind() const
{
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, m_RendererID));

}

void VertexBuffer::Unbind() const
{
	GLCall(glBindBuffer(GL_ARRAY_BUFFER, 0));

}