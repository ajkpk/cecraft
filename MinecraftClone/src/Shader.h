#pragma once

#include "glm/glm.hpp"

struct ShaderProgramSource
{
	std::string VertexSource;
	std::string FragmentSource;
};


class Shader
{
    private:
        std::string m_FilePath;
        unsigned int m_RendererID;

        //caching for uniforms
        std::unordered_map<std::string, int> m_UniformLocationCache;

        int GetUniformLocation(const std::string& name);

    public:
        Shader(const std::string& filepath);
        ~Shader();

        void Bind() const;
        void Unbind() const;

        ShaderProgramSource ParseShader(const std::string& filePath);
        unsigned int CreateShader(const std::string& vertexShader, const std::string& fragmentShader);
        unsigned int CompileShader(unsigned int type, const std::string& source);

        //Set uniforms
        void SetUniform4f(const std::string& name, float f0, float f1, float f2, float f3);
        void SetUniform1i(const std::string& name, int i);
        void SetUniformMat4f(const std::string& name, const glm::mat4& mat4);

};