#include "Log.h"

std::shared_ptr<spdlog::logger> Log::s_Logger;
std::shared_ptr<spdlog::logger> Log::s_TestLogger;
std::shared_ptr<spdlog::logger> Log::s_ShaderLogger;
std::shared_ptr<spdlog::logger> Log::s_EventLogger;


void Log::Init()
{
    spdlog::set_pattern("%^[%T] %n: %v%$");
    //s_Logger->set_level(spdlog::level::trace);
    s_Logger = spdlog::stdout_color_mt("[Cecraft]");
    s_TestLogger = spdlog::stdout_color_mt("[Test Framework]");
    s_ShaderLogger = spdlog::stdout_color_mt("[Shader System]");
    s_EventLogger = spdlog::stdout_color_mt("[Event System]");

}