#pragma once
#include "Window.h"
#include <GLFW/glfw3.h>

class WindowsWindow : public Window
{
    private:
        virtual void init(const WindowProps& props);
        virtual void shutdown();

        GLFWwindow* m_Window;

        struct WindowData
        {
            std::string title;
            unsigned int width;
            unsigned int height;
            bool vsync;
            EventCallbackFn eventCallback;
        } m_Data;

    public:
        WindowsWindow(const WindowProps& props);
        virtual ~WindowsWindow();

        void onUpdate() override;
        void setVSync(bool enabled) override;
        inline void setEventCallback(const EventCallbackFn& callback) override { m_Data.eventCallback = callback; }

        bool isVSync() const override;

        inline unsigned int getWidth() const override { return m_Data.width; }
        inline unsigned int getHeight() const override { return m_Data.height; }


};
