#pragma once
#include <functional>
#include <iostream>
#include <vector>
#include "Log.h"

namespace test
{
    class Test
    {
        public:
            Test() {}
            virtual ~Test(){}

            virtual void OnUpdate(float deltaTime){}
            virtual void OnRender() {}
            virtual void OnImGuiRender() {}
    };

    class TestMenu : public Test
    {
        private:
            Test*& m_CurrentTest;

            //Test should be created on button press -> lambda std::function returns a Test*() (new Test)
            std::vector<std::pair<std::string, std::function<Test*()>>> m_Tests;

        public:
            TestMenu(Test*& CurrenTestPointer);

            void OnImGuiRender() override;

            template<typename T>
            void RegisterTest(const std::string& name)
            {
                LOG_TEST_INFO("Reigstering test " + name);

                m_Tests.push_back(std::make_pair(name, [](){ return new T(); })); //lambda to create new Test T
            }
    };
}