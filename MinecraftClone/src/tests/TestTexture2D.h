#pragma once

#include "Test.h"
#include "../VertexBuffer.h"
#include "../VertexBufferLayout.h"
#include "../Texture.h"
#include <memory>


namespace test
{
    class TestTexture2D : public Test
    {
        private:
            glm::vec3 m_Translation;

            std::unique_ptr<VertexBuffer> m_VBO;
            std::unique_ptr<VertexArray> m_VAO;
            std::unique_ptr<IndexBuffer> m_IBO;
            std::unique_ptr<Shader> m_Shader;
            //std::unique_ptr<Texture> m_Texture;
            Texture* m_Texture; //TODO: does this have to be a normal pointer?

            glm::mat4 m_Proj, m_View;

            char m_Input[128];


        public:
            TestTexture2D();
            ~TestTexture2D() override;

            void OnUpdate(float deltaTime) override;
            void OnRender() override;
            void OnImGuiRender() override;
    };
}