#include "TestTexture2D.h"
#include "../Renderer.h"
#include "../Core.h"
#include "imgui/imgui.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"


namespace test
{
    TestTexture2D::TestTexture2D() : m_Translation(2, 2, 0), m_Proj(glm::ortho(0.0f, 1.0f, 0.0f, 1.0f, -1.0f, 1.0f)),
                                                            m_View(glm::translate(glm::mat4(1.0f), glm::vec3(0,0,0))), m_Input("")
    {

        float positions[] =
        {
            -0.25f, -0.25f, 0.0f, 0.0f,
            0.25f, -0.25f, 1.0f, 0.0f,
            0.25f, 0.25f, 1.0f, 1.0f,
            -0.25f, 0.25f, 0.0f, 1.0f
        };

        unsigned int indices[] = {
            0, 1, 2,
            2, 3, 0
        };


        m_VBO = std::make_unique<VertexBuffer>(positions, 4 * 4 * sizeof(float));
        VertexBufferLayout layout;
        layout.Push<float>(2);
        layout.Push<float>(2);

        m_VAO = std::make_unique<VertexArray>();
        m_VAO->AddBuffer(*m_VBO, layout);
        m_IBO = std::make_unique<IndexBuffer>(indices, 6);

		#ifdef CECRAFT_LINUX
			m_Texture = new Texture("MinecraftClone/res/textures/ground.png");//std::make_unique<Texture>("MinecraftClone/res/textures/ground.png");
			m_Shader = std::make_unique<Shader>("MinecraftClone/res/shaders/Basic.shader");
		#elif CECRAFT_WINDOWS
			m_Texture = new Texture("res/textures/ground.png");//std::make_unique<Texture>("MinecraftClone/res/textures/ground.png");
			m_Shader = std::make_unique<Shader>("res/shaders/Basic.shader");
        #else
            ASSERT(false, "Can't load shader. Platform not supported");
		#endif

        m_Shader->Bind();
        m_Shader->SetUniform4f("u_Color", 0.8f, 0.3f, 0.8f, 1.0f);
        m_Shader->SetUniform1i("u_Texture", 0);
    }

    TestTexture2D::~TestTexture2D()
    {
        delete m_Texture;
    }

    void TestTexture2D::OnUpdate(float deltaTime)
    {

    }

    void TestTexture2D::OnRender()
    {
        GLCall(glClearColor(0.0f, 0.0f, 0.0f, 0.0f));
        GLCall(glClear(GL_COLOR_BUFFER_BIT));

        Renderer renderer;

        m_Texture->Bind();


        glm::mat4 model = glm::translate(glm::mat4(1.0f), m_Translation);
        glm::mat4 mvp = m_Proj * m_View * model;

        m_Shader->Bind();
        m_Shader->SetUniformMat4f("u_MVP", mvp);
        renderer.Draw(*m_VAO, *m_IBO, *m_Shader);
    }

    void TestTexture2D::OnImGuiRender()
    {
        ImGui::InputText("Texture path", m_Input, 128);
        if(ImGui::Button("Open"))
        {
            m_Texture->Unbind();
            m_Shader->Unbind();
            delete m_Texture;
            m_Texture = new Texture(m_Input);
            m_Shader->Bind();
            m_Shader->SetUniform1i("u_Texture", 0);
        }

        ImGui::SliderFloat3("Translation", &m_Translation.x, 0.0f, 1.0f);
        ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
    }

}