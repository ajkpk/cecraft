#pragma once
#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "spdlog/fmt/ostr.h"

class Log
{
    private:
        static std::shared_ptr<spdlog::logger> s_Logger, s_TestLogger, s_ShaderLogger, s_EventLogger;

    public:
        static void Init();
        
        inline static std::shared_ptr<spdlog::logger> GetLogger() { return s_Logger; }
        inline static std::shared_ptr<spdlog::logger> GetTestLogger() { return s_TestLogger; }
        inline static std::shared_ptr<spdlog::logger> GetShaderLogger() { return s_ShaderLogger; }
        inline static std::shared_ptr<spdlog::logger> GetEventLogger() { return s_EventLogger; }

};

/* Logging Macros */
#define LOG_MAIN
#define LOG_TEST
#define LOG_SHADER
#define LOG_EVENT



#ifdef LOG_MAIN
    #define LOG_ERROR(...) Log::GetLogger()->error(__VA_ARGS__)
    #define LOG_WARN(...) Log::GetLogger()->warn(__VA_ARGS)
    #define LOG_INFO(...) Log::GetLogger()->info(__VA_ARGS__)
#else
    #define LOG_ERROR(...)
    #define LOG_WARN(...)
    #define LOG_INFO(...)
#endif

#ifdef LOG_TEST
    #define LOG_TEST_ERROR(...) Log::GetTestLogger()->error( __VA_ARGS__)
    #define LOG_TEST_WARN(...) Log::GetTestLogger()->warn(__VA_ARGS__)
    #define LOG_TEST_INFO(...) Log::GetTestLogger()->info(__VA_ARGS__)
#else
    #define LOG_TEST_ERROR(...)
    #define LOG_TEST_WARN(...)
    #define LOG_TEST_INFO(...)
#endif

#ifdef LOG_SHADER
    #define LOG_SHADER_ERROR(...) Log::GetShaderLogger()->error( __VA_ARGS__)
    #define LOG_SHADER_WARN(...) Log::GetShaderLogger()->warn(__VA_ARGS__)
    #define LOG_SHADER_INFO(...) Log::GetShaderLogger()->info(__VA_ARGS__)
#else
    #define LOG_SHADER_ERROR(...)
    #define LOG_SHADER_WARN(...)
    #define LOG_SHADER_INFO(...)
#endif


#ifdef LOG_EVENT
    #define LOG_EVENT_ERROR(...) Log::GetEventLogger()->error( __VA_ARGS__)
    #define LOG_EVENT_WARN(...) Log::GetEventLogger()->warn(__VA_ARGS__)
    #define LOG_EVENT_INFO(...) Log::GetEventLogger()->info(__VA_ARGS__)
#else
    #define LOG_EVENT_ERROR(...)
    #define LOG_EVENT_WARN(...)
    #define LOG_EVENT_INFO(...)
#endif