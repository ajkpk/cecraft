#include "cpch.h"
#include "Cecraft.h"
#include "Log.h"
#include <glad/glad.h>
#include "layers/ImGuiLayer.h"

#define BIND_EVENT_FUNC(x) std::bind(&x, this, std::placeholders::_1)

Cecraft* Cecraft::s_Instance = nullptr;


class ExampleLayer : public Layer
{
    public:
        ExampleLayer() : Layer("Example") {}

        void onUpdate() override
        {
            LOG_INFO("Layer::update");
        }

        void onEvent(Event& e) override
        {
            LOG_INFO("{0}", e);
        }
};



Cecraft::Cecraft()
{
    ASSERT(!s_Instance, "Singleton duplicated");
    s_Instance = this;

    m_Window = std::unique_ptr<Window>(Window::create());
    m_Window->setEventCallback(BIND_EVENT_FUNC(Cecraft::onEvent));

    pushLayer(new ExampleLayer());
    pushOverlay(new ImGuiLayer());
}

Cecraft::~Cecraft()
{

}

void Cecraft::run()
{
    while(m_Running)
    {
        glClearColor(1, 0, 1, 1);
        glClear(GL_COLOR_BUFFER_BIT);

        for(Layer* layer : m_LayerStack)
            layer->onUpdate();

        m_Window->onUpdate();
    }
}

void Cecraft::onEvent(Event& e)
{
    //LOG_INFO(e);
    EventDispatcher dispatcher(e);
    dispatcher.dispatch<WindowCloseEvent>(BIND_EVENT_FUNC(Cecraft::onWindowClose));

    for(auto it = m_LayerStack.end(); it != m_LayerStack.begin(); )
    {
        (*--it)->onEvent(e);

        if(e.handled())
            break;
    }
}

bool Cecraft::onWindowClose(WindowCloseEvent& e)
{
    m_Running = false;
    return true;
}

void Cecraft::pushLayer(Layer* layer)
{
    m_LayerStack.pushLayer(layer);
    layer->onAttach();
}

void Cecraft::pushOverlay(Layer* layer)
{
    m_LayerStack.pushOverlay(layer);
    layer->onAttach();
}