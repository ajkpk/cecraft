#include "cpch.h"
#include "Core.h"
#include "Log.h"

#include "GL/glew.h"
#include "GLFW/glfw3.h"

#ifdef TRUE
#include "Renderer.h"
#include "VertexArray.h"
#include "VertexBuffer.h"
#include "VertexBufferLayout.h"
#include "IndexBuffer.h"
#include "Shader.h"
#include "Texture.h"

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw_gl3.h"

#include "tests/Test.h"
#include "tests/TestClearColor.h"
#include "tests/TestTexture2D.h"
#endif

#include "Cecraft.h"



int Setup(GLFWwindow*& window)
{
	LOG_INFO("Initializing GLFW!");


		/* Initialize the library */
	if (!glfwInit())
		return -1;


	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);



	/* Create a windowed mode window and its OpenGL context */
	window = glfwCreateWindow(640, 480, "Cecraft", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	glfwSwapInterval(1);

	LOG_INFO("Initializing GLEW!");	

	if (glewInit() != GLEW_OK)
	{
		LOG_ERROR("GLEW error");
		std::cin.get();
		return -1;
	}
	else
	{
		LOG_INFO("GLEW Version: {0}", glGetString(GL_VERSION));
	}


	GLCall(glEnable(GL_BLEND));
	GLCall(glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA));
	
	return 0;
}

int main()
{
	Log::Init();

	Cecraft* app = new Cecraft();
	app->run();

	return 0;

#ifdef TRUE
	LOG_INFO("Initialized Log!");

	GLFWwindow* window;

	if(Setup(window) != 0) return -1;

	{


		Renderer renderer;


    	ImGui::CreateContext();
    	ImGui_ImplGlfwGL3_Init(window, true);
    	ImGui::StyleColorsDark();

		//test::TestClearColor test;

		test::Test* currentTest = nullptr;
		test::TestMenu* testMenu = new test::TestMenu(currentTest);
		currentTest = testMenu;
		
		testMenu->RegisterTest<test::TestClearColor>("Clear Color");
		testMenu->RegisterTest<test::TestTexture2D>("2D Texture");


		while (!glfwWindowShouldClose(window))
		{
			renderer.Clear();

	        ImGui_ImplGlfwGL3_NewFrame();

			if(currentTest)
			{
				currentTest->OnUpdate(0.0f);
				currentTest->OnRender();

				ImGui::Begin("Test");
				if(currentTest != testMenu && ImGui::Button("<-"))
				{
					delete currentTest;
					currentTest = testMenu;
				}
				currentTest->OnImGuiRender();
				ImGui::End();
			}

			ImGui::Render();
        	ImGui_ImplGlfwGL3_RenderDrawData(ImGui::GetDrawData());

			glfwSwapBuffers(window);
			glfwPollEvents();
		}

		if(currentTest != testMenu)
			delete testMenu;
		delete currentTest;
	}


	ImGui_ImplGlfwGL3_Shutdown();
	ImGui::DestroyContext();

	glfwTerminate();

	
	return 0;
	#endif
}