#include "cpch.h"
#include "platform/opengl/ImGuiRenderer.h"
#include "ImGuiLayer.h"
#include "imgui.h"
#include <GLFW/glfw3.h>
#include "Cecraft.h"


ImGuiLayer::ImGuiLayer() : Layer("ImGui")
{

}

ImGuiLayer::~ImGuiLayer()
{

}

void ImGuiLayer::onAttach() 
{
    ImGui::CreateContext();
    ImGui::StyleColorsDark();

    ImGuiIO& io = ImGui::GetIO();
    io.BackendFlags |= ImGuiBackendFlags_HasMouseCursors;
    io.BackendFlags |= ImGuiBackendFlags_HasSetMousePos;

    io.KeyMap[ImGuiKey_Tab] = GLFW_KEY_TAB;
    io.KeyMap[ImGuiKey_LeftArrow] = GLFW_KEY_LEFT;
    io.KeyMap[ImGuiKey_RightArrow] = GLFW_KEY_RIGHT;
    io.KeyMap[ImGuiKey_UpArrow] = GLFW_KEY_UP;
    io.KeyMap[ImGuiKey_DownArrow] = GLFW_KEY_DOWN;
    io.KeyMap[ImGuiKey_PageUp] = GLFW_KEY_PAGE_UP;
    io.KeyMap[ImGuiKey_PageDown] = GLFW_KEY_PAGE_DOWN;
    io.KeyMap[ImGuiKey_Home] = GLFW_KEY_HOME;
    io.KeyMap[ImGuiKey_End] = GLFW_KEY_END;
    io.KeyMap[ImGuiKey_Insert] = GLFW_KEY_INSERT;
    io.KeyMap[ImGuiKey_Delete] = GLFW_KEY_DELETE;
    io.KeyMap[ImGuiKey_Backspace] = GLFW_KEY_BACKSPACE;
    io.KeyMap[ImGuiKey_Space] = GLFW_KEY_SPACE;
    io.KeyMap[ImGuiKey_Enter] = GLFW_KEY_ENTER;
    io.KeyMap[ImGuiKey_Escape] = GLFW_KEY_ESCAPE;
    io.KeyMap[ImGuiKey_A] = GLFW_KEY_A;
    io.KeyMap[ImGuiKey_C] = GLFW_KEY_C;
    io.KeyMap[ImGuiKey_V] = GLFW_KEY_V;
    io.KeyMap[ImGuiKey_X] = GLFW_KEY_X;
    io.KeyMap[ImGuiKey_Y] = GLFW_KEY_Y;
    io.KeyMap[ImGuiKey_Z] = GLFW_KEY_Z;

    #ifdef CECRAFT_LINUX
        ImGui_ImplOpenGL3_Init("#version 130");
    #else
        ImGui_ImplOpenGL3_Init("#version 410");
    #endif
}

void ImGuiLayer::onDetach() {}

void ImGuiLayer::onUpdate() 
{
    
    ImGuiIO& io = ImGui::GetIO();
    

    Cecraft& app = Cecraft::getInstance();

    

    io.DisplaySize = ImVec2(app.getWindow().getWidth(), app.getWindow().getHeight());

    

    float time = (float)glfwGetTime();
    io.DeltaTime = m_Time > 0.0f ? (time - m_Time) : (1.0f / 60.0f);
    m_Time = time;

    

    ImGui_ImplOpenGL3_NewFrame();
    ImGui::NewFrame();

    static bool show = true;
    ImGui::ShowDemoWindow(&show);

    

    ImGui::Render();
    

    ImDrawData* test = ImGui::GetDrawData();
    

    ImGui_ImplOpenGL3_RenderDrawData(test);
    

}

void ImGuiLayer::onEvent(Event& e) {}
