#pragma once
#include "Layer.h"
#include "events/Event.h"

class ImGuiLayer : public Layer
{
    private:
        float m_Time = 0.0f;

    public:
        ImGuiLayer();
        ~ImGuiLayer();


        void onAttach();
        void onDetach();
        void onUpdate();
        void onEvent(Event& e);

};