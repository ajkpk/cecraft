#pragma once
#include "Log.h"

#if !defined(CECRAFT_LINUX) && !defined(CECRAFT_WINDOWS)
	#define CECRAFT_LINUX 1 //just so the editor doesn't freak out
#endif


#ifdef CECRAFT_WINDOWS
	#define ASSERT(x, ...) if(!(x)){ LOG_ERROR("Assertion failed: {0}", __VA_ARGS__); __debugbreak(); }
#elif CECRAFT_LINUX
	#define ASSERT(x, ...) if(!(x)){ LOG_ERROR("Assertion failed: {0}", __VA_ARGS__); __builtin_trap(); }
#endif

#define GLCall(x) 	GLClearError();\
					x;\
					ASSERT(GLLogCall(#x, __FILE__, __LINE__), "Error during GLCall")


void GLClearError();

bool GLLogCall(const char* function, const char* file, int line);