#pragma once
#include <string>
#include <functional>

#define BIT(x) (1 << x)



enum class EventType
{
    None = 0,
    WindowClose, WindowResize, WindowFocus, WindowLostFocus, WindowMoved,
    AppTick, AppUpdate, AppRender,
    KeyPressed, KeyReleased,
    MouseButtonPressed, MouseButtonReleased, MouseMoved, MouseScrolled
};

/* Setting a bit mask to filter event categories (i.e. Keyboard, Mouse, Button is in Input*/
enum EventCategory
{
    None = 0,
    Application     = BIT(0),
    Input           = BIT(1),
    Keyboard        = BIT(2),
    Mouse           = BIT(3),
    Button          = BIT(4)
};

class Event
{
    friend class EventDispatcher;

    protected:
        bool m_Handled = false;


    public:
        virtual EventType GetEventType() const = 0; //pure virtual -> needs to be defined
        virtual const char* GetName() const = 0;
        virtual int GetCategoryFlags() const = 0;
        virtual std::string ToString() const { return GetName(); } //default GetName can be overridden

        inline bool IsInCategory(EventCategory category)
        {
            return GetCategoryFlags() & category;
        }

        bool handled() { return m_Handled; }
};

class EventDispatcher
{
    template<typename T>
    using EventFn = std::function<bool(T&)>;

    private:
        Event& m_Event;


    public:
        EventDispatcher(Event& event) : m_Event(event) {}

        template<typename T>
        bool dispatch(EventFn<T> func)
        {
            if(m_Event.GetEventType() == T::GetStaticType())
            {
                m_Event.m_Handled = func(*(T*)&m_Event);
                return true;
            }

            return false;
        }
};

inline std::ostream& operator << (std::ostream& os, const Event& e)
{
    return os << e.ToString();
}

#define EVENT_CLASS_TYPE(type) static EventType GetStaticType() { return EventType::type; }\
                                virtual EventType GetEventType() const override { return GetStaticType(); }\
                                virtual const char* GetName() const override { return #type; }

#define EVENT_CLASS_CATEGORY(category) virtual int GetCategoryFlags() const override { return category; }