#pragma once
#include <sstream>
#include "Event.h"

class MouseMovedEvent : public Event
{
    private:
        float m_MouseX, m_MouseY;

    public:
        MouseMovedEvent(float x, float y) : m_MouseX(x), m_MouseY(y) {}

        inline float GetX() const { return m_MouseX; }
        inline float GetY() const { return m_MouseY; }

        std::string ToString() const override
        {
            std::stringstream ss;
            ss << "MouseMovedEvent: " << m_MouseX << ", " << m_MouseY;
            return ss.str();
        }

        EVENT_CLASS_TYPE(MouseMoved)
        EVENT_CLASS_CATEGORY(Mouse | Input);
};

class MouseScrolledEvent : public Event
{
    private:
        float m_OffsetX, m_OffsetY;

    public:
        MouseScrolledEvent(float x, float y) : m_OffsetX(x), m_OffsetY(y) {}

        inline float GetX() const { return m_OffsetX; }
        inline float GetY() const { return m_OffsetY; }

        std::string ToString() const override
        {
            std::stringstream ss;
            ss << "MouseScrolledEvent: " << m_OffsetX << ", " << m_OffsetY;
            return ss.str();
        }

        EVENT_CLASS_TYPE(MouseScrolled)
        EVENT_CLASS_CATEGORY(Mouse | Input);
};

class MouseButtonEvent : public Event
{
    protected:
        MouseButtonEvent(int button) : m_Button(button) {}
        
        int m_Button;

    public:
        inline int GetMouseButton() const { return m_Button; }

        EVENT_CLASS_CATEGORY(Mouse | Input);
};

class MouseButtonPressedEvent : public MouseButtonEvent
{
    public:
        MouseButtonPressedEvent(int button) : MouseButtonEvent(button) {}

        std::string ToString() const override
        {
            std::stringstream ss;
            ss << "MouseButtonPressedEvent: " << m_Button;
            return ss.str();
        }

        EVENT_CLASS_TYPE(MouseButtonPressed)
};

class MouseButtonReleasedEvent : public MouseButtonEvent
{
    public:
        MouseButtonReleasedEvent(int button) : MouseButtonEvent(button) {}

        std::string ToString() const override
        {
            std::stringstream ss;
            ss << "MouseButtonReleasedEvent: " << m_Button;
            return ss.str();
        }

        EVENT_CLASS_TYPE(MouseButtonReleased)
};