#pragma once
#include "events/Event.h"
#include "events/ApplicationEvent.h"
#include "Window.h"
#include "LayerStack.h"

class Cecraft
{
    private:
        static Cecraft* s_Instance;

        std::unique_ptr<Window> m_Window;

        bool m_Running = true;

        void onEvent(Event& e);

        bool onWindowClose(WindowCloseEvent& e);

        LayerStack m_LayerStack;

    public:
        Cecraft();
        virtual ~Cecraft();

        void run();
        void pushLayer(Layer* layer);
        void pushOverlay(Layer* layer);

        inline Window& getWindow() { return *m_Window; }
        inline static Cecraft& getInstance() { return *s_Instance; }
};
