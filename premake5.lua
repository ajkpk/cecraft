workspace "cecraft"
    architecture "x64"


    configurations
    {
        "Debug",
        "Release"
    }

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

include "MinecraftClone/vendor/glad"
include "MinecraftClone/vendor/imgui"

project "MinecraftClone"
    location "MinecraftClone"
    kind "ConsoleApp"
    language "C++"

    targetdir ("bin/" .. outputdir .. "/MinecraftClone")
    objdir ("bin-int/" .. outputdir .. "/MinecraftClone")

    pchheader "cpch.h"
    pchsource "MinecraftClone/src/cpch.cpp"



    files
    {
        "MinecraftClone/src/**.h",
        "MinecraftClone/src/**.cpp",
        "MinecraftClone/vendor/glm/*.hpp",
        "MinecraftClone/vendor/stb_image/*.cpp",
        "MinecraftClone/vendor/stb_image/*.h"
    }

    includedirs
    {
        "MinecraftClone/src/",
        "MinecraftClone/vendor/spdlog/include",
        "MinecraftClone/vendor/imgui",
        "MinecraftClone/vendor/stb_image",
        "MinecraftClone/vendor/",
        "Dependencies/GLEW/include",
        "Dependencies/GLFW/include",
        "MinecraftClone/vendor/glad/include"
    }

    filter "system:windows"
        cppdialect "C++17"
        staticruntime "On"
        systemversion "latest"

        libdirs{"Dependencies/GLEW/lib/Release/x64", "Dependencies/GLFW/lib-vc2017"}
        links{"glfw3.lib", "Glad", "ImGui", "opengl32.lib"}

        defines { "CECRAFT_WINDOWS" }

    filter "system:linux"
        cppdialect "C++14"
        staticruntime "On"
        systemversion "latest"

        libdirs{"Dependencies/GLEW/lib/Release/linux", "Dependencies/GLFW/lib-vc2017/linux"}
        links{"GL", "GLU", "glut", "GLEW", "Glad", "ImGui", "glfw3", "dl", "X11", "m", "pthread"}

        defines { "CECRAFT_LINUX", "GLFW_INCLUDE_NONE" }

    
    filter "configurations:Debug"
        defines "_DEBUG"
        symbols "On"
    
    filter "configurations:Release"
        defines "_RELEASE"
        optimize "On"

    filter {"system:windows", "configurations:Release"}
        buildoptions "/MT"