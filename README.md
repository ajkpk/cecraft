## Cecraft

A try to create a Minecraft clone for Windows and Linux in C++ using OpenGL _et al_.

---

**Used libraries**
- GLEW, GLFW3, GL, GLU, glut  (accessing OpenGL)

- glm, m (maths library)

- dl (dynamic linking library)

- X11 (window library)

- pthread (multithreading library)

- stb_image (textures)

- ImGui (debug gui and menus)
